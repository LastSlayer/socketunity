using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoteController : MonoBehaviour
{
    // THIS CLASS REPRESENTS ACTUAL FUNCTION TO PERFORM IN PROGRAM
    // IT STORE MESSAGE DECIPHER FROM BYTE ARRAY AND PERFORM ACTION DEPEND ON THAT MESSAGE
    // IN THIS CASE THIS FUNCTION WILL STORE THE MESSAGE AND SHOW IT ON UI

    public static RemoteController Instance { private set; get; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(this);

        Instance = this;
    }

    private string registeredMessage;
    public void RegisterMessage(string message)
    {
        // SINCE WE UPDATE UI SO WE NEED TO JOIN UI THREAD
        ThreadInvoker.Instance.AddQueue(() => 
        {
            SocketUIController.Instance.DisplaySocketMessage(message);
        });
    }
}

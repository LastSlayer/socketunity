using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SocketUIController : MonoBehaviour
{
    // THIS CLASS WILL MANAGE UI ELEMENTS IN SCENE
    // TO USE THIS CLASS'S FUNCTION CALL IT FROM THREADINVOKER

    public static SocketUIController Instance { private set; get; }
    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(this);

        Instance = this;
    }

    public Text connectionStatus;
    public Text socketMessage;
    public GameObject stopServerButton;
    public GameObject startServerButton;
    public GameObject serverStatusText;
    public Image UiMask;

    private bool isConnected = false;

    public void OnServerStart()
    {
        stopServerButton.SetActive(true);
        serverStatusText.SetActive(true);
        startServerButton.SetActive(false);
    }

    public void OnServerStop()
    {
        stopServerButton.SetActive(false);
        serverStatusText.SetActive(false);
        startServerButton.SetActive(true);
    }

    public void OnConnection()
    {
        UiMask.gameObject.SetActive(false);
        connectionStatus.color = Color.green;
        connectionStatus.text = "Remote Controller Connected";
    }

    public void OnDisconnection()
    {
        UiMask.gameObject.SetActive(true);
        connectionStatus.color = Color.red;
        connectionStatus.text = "Remote Controller Disconnected";
    }

    public void DisplaySocketMessage(string message)
    {
        socketMessage.text = message;
    }
}

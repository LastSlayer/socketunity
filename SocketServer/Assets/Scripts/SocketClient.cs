using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

public class SocketClient
{
    // USED TO REPRESENT CONNECTED CLIENT INSTANCE
    // CONTAIN ID, TCPCLIENT AND NETWORKSTREAM
    // CREATED THIS CLASS'S INSTANCE AND USE STARTLISTENING TO CREATE LISTENING LOOP THREAD IN SOCKETSERVERCONTROLLER

    public int ID; // Client ID for identification
    public TcpClient tcpClient; // client identification
    public NetworkStream stream; // data receiving

    private byte[] readBuffer = new byte[4096]; // message decode from network stream

    #region LISTENING
    public void StartListening()
    {
        // Read incoming message and upon receiving called Async callback function
        stream.BeginRead(readBuffer, 0, readBuffer.Length, new AsyncCallback(OnMessageReceiveCallback), null);
    }

    private void OnMessageReceiveCallback(IAsyncResult asyncResult)
    {
        #region CHECK FOR DISCONNECTION
        int size = stream.EndRead(asyncResult); // Normally when client disconnect it sent empty message hence checking for empty response
        if(size < 1)
        {
            Debug.Log("[Server] Client disconnected");
            SocketServerController.clientList.Remove(this);
            Debug.Log(SocketServerController.clientList.Count);
            return;
        }
        #endregion

        #region MESSAGE RECEIVER
        string message = Encoding.ASCII.GetString(readBuffer);

        // ========== MESSAGE EDITOR ========== //
        message = message.Trim('\0');
        Debug.Log("[Server] Received message : " + message);
        #endregion

        #region HEADER SPLITTER
        // ========== HEADER DECIPHER========== //
        string[] splited = message.Split('|');
        string header = splited[0];
        string socketMessage = splited[1];
        switch (header)
        {
            case "CONNECTION":
                if(socketMessage == "0")
                    ThreadInvoker.Instance.AddQueue(SocketUIController.Instance.OnDisconnection);
                if (socketMessage == "1")
                    ThreadInvoker.Instance.AddQueue(SocketUIController.Instance.OnConnection);
                break;
            case "MESSAGE":
                
                ThreadInvoker.Instance.AddQueue(() =>
                {
                    RemoteController.Instance.RegisterMessage(socketMessage);
                }
                );
                break;
        }
        #endregion

        #region UPDATE ALL CONNECTED CLIENT
        int clientIndex = 0; // Used for debug client index
        foreach(SocketClient client in SocketServerController.clientList)
        {
            Debug.Log("[Server] Update Message for client : " + clientIndex);

            NetworkStream sendStream = client.stream;
            WriteMessage(message, sendStream);
        }
        #endregion

        // Reset buffer and loop listening
        Array.Clear(readBuffer, 0, readBuffer.Length);
        StartListening();
    }
    #endregion

    #region SENDING
    private void WriteMessage(string message)
    {
        byte[] sendBytes = Encoding.ASCII.GetBytes(message);
        stream.Write( sendBytes, 0, sendBytes.Length);
        stream.Flush();
    }

    private void WriteMessage(string message, NetworkStream sendingStream)
    {
        byte[] sendBytes = Encoding.ASCII.GetBytes(message);
        sendingStream.Write(sendBytes, 0, sendBytes.Length);
        sendingStream.Flush();
    }
    #endregion
}

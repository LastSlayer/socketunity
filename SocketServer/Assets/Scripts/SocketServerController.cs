using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;

public class SocketServerController : MonoBehaviour
{
    public static SocketServerController Instance { private set; get; }

    public static List<SocketClient> clientList = new List<SocketClient>();
    TcpListener serverInstance;
    [SerializeField]
    private int PORT = 5555;
    private int ID = 0;
    private Thread t;

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(this);

        Instance = this;
    }

    private void StartServer()
    {
        // START SERVER
        TcpListener tcpServer = new TcpListener(IPAddress.Any, PORT);
        serverInstance = tcpServer;
        tcpServer.Start();
        Debug.Log("[Server] Start at port : " + PORT);

        // UPDATE UI
        ThreadInvoker.Instance.AddQueue(SocketUIController.Instance.OnServerStart);

        // START CLIENT LISTENER LOOP
        while (true)
        {
            // LISTEN FOR CLIENT CONNECTION
            TcpClient incomingClient = tcpServer.AcceptTcpClient(); // THIS WILL WAIT FOR CLIENT CONNECTION
            // CONTINUE AFTER CLIENT CONNECTED
            Debug.Log("[Server] Client Connected");

            // CREATE NEW CLIENT INSTANCE
            SocketClient client = new SocketClient
            {
                ID = ID++,
                tcpClient = incomingClient,
                stream = incomingClient.GetStream()
            };

            // START LISTENING TO CLIENT (CREATE NEW THREAD IN SOCKETCLIENT CLASS)
            client.StartListening();
            // ADD CLIENT TO CONNECTED CLIENT LIST
            clientList.Add(client);
        }
    }

    public void StopServerButton()
    {
        // STOP AND JOIN SERVER THREAD
        t.Abort();
        t.Join();
        // CLOSE ALL CLIENT SOCKET
        clientList.ForEach(c => c.stream.Close());
        // STOP SERVER
        serverInstance.Stop();

        // UI UPDATE
        ThreadInvoker.Instance.AddQueue(SocketUIController.Instance.OnServerStop);
        ThreadInvoker.Instance.AddQueue(SocketUIController.Instance.OnDisconnection);
    }

    public void StartServerButton()
    {
        // START SERVER THREAD
        t = new Thread(StartServer);
        t.Start();

        // UI UPDATE
        ThreadInvoker.Instance.AddQueue(SocketUIController.Instance.OnServerStart);
    }

    private void Start()
    {
        // RESET UI ELEMENTS
        SocketUIController.Instance.OnDisconnection();
    }

    private void Update()
    {
        // UPDATE UI ELEMENT WHEN THERE IS NO CLIENT CONNECTION
        if (clientList.Count == 0)
            SocketUIController.Instance.OnDisconnection();
    }
}

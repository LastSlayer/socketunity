using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SocketUIController : MonoBehaviour
{
    public static SocketUIController Instance { private set; get; }
    public Text connectionStatus;
    public GameObject connectionButton;
    public GameObject disconnectionButton;
    public GameObject sendMessageButton;
    public Image UiMask;

    private bool isConnected = false;

    private void Awake()
    {
        Instance = this;
    }

    public void OnConnection()
    {
        //UiMask.gameObject.SetActive(false);
        connectionStatus.color = Color.green;
        connectionStatus.text = "Connected to Project";
        sendMessageButton.SetActive(true);
        connectionButton.SetActive(false);
        disconnectionButton.SetActive(true);
    }

    public void OnDisconnection()
    {
        //UiMask.gameObject.SetActive(true);
        connectionStatus.color = Color.red;
        connectionStatus.text = "Disconnected";
        sendMessageButton.SetActive(false);
        connectionButton.SetActive(true);
        disconnectionButton.SetActive(false);
    }
}

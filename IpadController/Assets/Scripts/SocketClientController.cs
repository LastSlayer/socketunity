using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;


public class SocketClientController : MonoBehaviour
{
    public static SocketClientController Instance { private set; get; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
            Destroy(this);

        Instance = this;
    }

    [SerializeField]
    private string serverIP = "127.0.0.1";
    [SerializeField]
    private int serverPort = 4869;


    private TcpClient tcpClient = new TcpClient();
    private NetworkStream stream;
    private byte[] readBuffer = new byte[4869];
    private int ID = -1;

    #region LISTENING
    public void StartListening()
    {
        // Read incoming message and upon receiving called Async callback function
        stream.BeginRead(readBuffer, 0, readBuffer.Length, new AsyncCallback(OnMessageReceiveCallback), null);
    }

    private void OnMessageReceiveCallback(IAsyncResult asyncResult)
    {
        #region CHECK FOR DISCONNECTION
        int size = stream.EndRead(asyncResult); // Normally when client disconnect it sent empty message hence checking for empty response
        if (size < 1)
        {
            Debug.Log("[Server] Can't connect to server");
            ThreadInvoker.Instance.AddQueue(SocketUIController.Instance.OnDisconnection);
            return;
        }
        #endregion

        #region MESSAGE RECEIVER
        string message = Encoding.ASCII.GetString(readBuffer);

        // ========== MESSAGE EDITOR ========== //
        message = message.Trim('\0');
        Debug.Log("[Server] Received message : " + message);
        #endregion

        // ========== HEADER DECIPHER========== //
        string[] splited = message.Split('|');
        string header = splited[0];
        string socketMessage = splited[1];
        Debug.Log("SOCKETMESSAGE[1] : " + socketMessage);
        switch (header)
        {
            case "CONNECTION":
                if (socketMessage == "0")
                    ThreadInvoker.Instance.AddQueue(SocketUIController.Instance.OnDisconnection);
                if (socketMessage == "1")
                    ThreadInvoker.Instance.AddQueue(SocketUIController.Instance.OnConnection);
                break;
        }

        // Reset buffer and loop listening
        Array.Clear(readBuffer, 0, readBuffer.Length);
        StartListening();
    }
    #endregion

    #region SENDING
    private void WriteMessage(string message)
    {
        byte[] sendBytes = Encoding.ASCII.GetBytes(message);
        stream.Write(sendBytes, 0, sendBytes.Length);
        stream.Flush();
    }

    private void WriteMessage(string message, NetworkStream sendingStream)
    {
        byte[] sendBytes = Encoding.ASCII.GetBytes(message);
        sendingStream.Write(sendBytes, 0, sendBytes.Length);
        sendingStream.Flush();
    }
    #endregion

    #region BUTTON FUNCTION
    public void ConnectButton()
    {
        tcpClient = new TcpClient();
        tcpClient.Connect(serverIP, serverPort);

        if (tcpClient.Connected)
        {
            stream = tcpClient.GetStream();
            WriteMessage("CONNECTION|1");
            ThreadInvoker.Instance.AddQueue(SocketUIController.Instance.OnConnection);
            StartListening();
        }
    }

    public void DisconnectButton()
    {
        if (!tcpClient.Connected) return;
        ThreadInvoker.Instance.AddQueue(SocketUIController.Instance.OnDisconnection);
        tcpClient.Close();
        tcpClient.Dispose();
    }

    public void MessageSendButton()
    {

        string packedMessage = "MESSAGE|FUCKYOU";
        byte[] sendBytes = Encoding.ASCII.GetBytes(packedMessage);
        stream.Write(sendBytes, 0, sendBytes.Length);
        stream.Flush();
        /*
        Thread t = new Thread(StartListening);
        t.Start();*/
    }
    #endregion

}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThreadInvoker : MonoBehaviour
{
    public static ThreadInvoker Instance { private set; get; }

    private Queue<Action> Actions = new Queue<Action>();

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
    }


    public void AddQueue(Action action)
    {
        Actions.Enqueue(action);
    }


    void Update()
    {
        while(Actions.Count > 0)
        {
            Actions.Dequeue()();
        }
    }
}
